package tp_domain_project.Factories;

import tp_domain_project.Domains.Customer;

public class CustomerFactory {
    /*
        private int customerNumber;
        private String id;
        private String name;
        private String surname;
        private String contactNumber;
        private int accountID;
     */

    public static Customer getCustomer(int customerNumber, String id, String name, String surname, String contactNumber, int accountID){
        Customer customerFactory = new Customer.Builder()
                .customerNumber(customerNumber)
                .id(id)
                .name(name)
                .surname(surname)
                .contactNumber(contactNumber)
                .accountID(accountID)
                .build();
        return customerFactory;
    }
}
