package tp_domain_project.Factories;

import tp_domain_project.Domains.Account;

public class AccountFactory {
    /*
    private int accountID;
    private double balance;
    private String password;
    private String accountType;
     */
    public static Account getAccount(int accountID,long balance, String password, String accountType){
        Account accountFactory = new Account.Builder()
                .accountID(accountID)
                .balance(balance)
                .password(password)
                .accountType(accountType)
                .build();
        return accountFactory;
    }
}
