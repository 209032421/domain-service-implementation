package tp_domain_project.Factories;

import tp_domain_project.Domains.Bank;

public class BankFactory {
    /*
    private int branchCode;
    private String address;
    private String telephone;
     */

    public static Bank getBank(int branchCode, String address, String telephone){
        Bank bankFactory = new Bank.Builder()
                .branchCode(branchCode)
                .address(address)
                .telephone(telephone)
                .build();
        return bankFactory;
    }
}
