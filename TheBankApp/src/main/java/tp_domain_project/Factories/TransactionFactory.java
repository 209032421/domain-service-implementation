package tp_domain_project.Factories;

import tp_domain_project.Domains.Transaction;

import java.util.Date;

public class TransactionFactory {

    /*
    private int transactionID;
    private Date transactionDate;
    private String transactionName;
    private int accountID;
     */

    public static Transaction getTransaction(int transactionID, Date transactionDate,
                                             String transactionName, int accountID){
        Transaction transactionFactory = new Transaction.Builder()
                .transactionID(transactionID)
                .transactionDate(transactionDate)
                .transactionName(transactionName)
                .accountID(accountID)
                .build();
        return transactionFactory;
    }
}
