package tp_domain_project.Domains;

import java.io.Serializable;

public class Customer implements Serializable{

    private int customerNumber;
    private String id;
    private String name;
    private String surname;
    private String contactNumber;
    private int accountID;//FK


    //getters
    public int getCustomerNumber() {
        return customerNumber;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public int getAccountID() {return accountID;}



    //constructor
    public Customer() {
    }

//builder constructor
    public Customer(Builder builder){
        this.customerNumber=builder.customerNumber;
        this.id=builder.id;
        this.name=builder.name;
        this.surname=builder.surname;
        this.contactNumber=builder.contactNumber;
        this.accountID=builder.accountID;
    }

    public static class Builder{
        private int customerNumber;
        private String id;
        private String name;
        private String surname;
        private String contactNumber;
        private int accountID;

        public  Builder accountID(int value){
            this.accountID=value;
            return this;
        }

        public Builder customerNumber(int value){
            this.customerNumber = value;
            return this;
        }

        public Builder id(String value){
            this.id = value;
            return this;
        }

        public Builder name(String value){
            this.name = value;
            return this;
        }

        public Builder surname(String value){
            this.surname = value;
            return this;
        }

        public Builder contactNumber(String value){
            this.contactNumber = value;
            return this;
        }

        public Builder copy(Customer c) {
            this.id = c.id;
            this.name=c.name;
            this.surname=c.surname;
            this.contactNumber=c.contactNumber;
            return this;
        }

        public Customer build(){
            return new Customer(this);
        }

    }
}
