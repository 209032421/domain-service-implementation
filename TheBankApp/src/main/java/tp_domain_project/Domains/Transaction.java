package tp_domain_project.Domains;

import java.io.Serializable;
import java.util.Date;

public class Transaction implements Serializable{

    private int transactionID;
    private Date transactionDate;
    private String transactionName;
    private int accountID;//FK

    public int getTransactionID() {
        return transactionID;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public int getAccountID() {
        return accountID;
    }

    public Transaction() {
    }

    //builder constructor
    public Transaction(Builder builder){
        this.transactionID=builder.transactionID;
        this.transactionDate=builder.transactionDate;
        this.transactionName=builder.transactionName;
        this.accountID=builder.accountID;

    }

    public static class Builder{
        private int transactionID;
        private Date transactionDate;
        private String transactionName;
        private int accountID;

        public Builder transactionID(int value){
            this.transactionID = value;
            return this;
        }

        public Builder transactionDate(Date value){
            this.transactionDate = value;
            return this;
        }

        public Builder transactionName(String value){
            this.transactionName = value;
            return this;
        }

        public Builder accountID(int value){
            this.accountID = value;
            return this;
        }

        public Transaction build(){
            return new Transaction(this);
        }

    }

}
