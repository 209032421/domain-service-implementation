package tp_domain_project.Domains;

import java.io.Serializable;

public class Bank implements Serializable{

    private int branchCode;
    private String address;
    private String telephone;


    public Integer getBranchCode() {
        return branchCode;
    }

    public String getAddress() {
        return address;
    }

    public String getTelephone() {
        return telephone;
    }

    public Bank() {
    }

    public Bank(Builder builder) {
        this.branchCode = builder.branchCode;
        this.telephone = builder.telephone;
        this.address = builder.address;
    }

    public static class Builder {
        private int branchCode;
        private String telephone;
        private String address;

        public Builder branchCode(int value) {
            this.branchCode = value;
            return this;
        }

        public Builder telephone(String value) {
            this.telephone = value;
            return this;
        }

        public Builder address(String value) {
            this.address = value;
            return this;
        }

        public Bank build(){
            return new Bank(this);
        }

    }
}
