package tp_domain_project.Domains;


import java.io.Serializable;

public class Account implements Serializable{

    private int accountID;
    private double balance;
    private String password;
    private String accountType;

    public int getAccountID() {
        return accountID;
    }

    public double getBalance() {
        return balance;
    }

    public String getPassword() {
        return password;
    }

    public String getAccountType() {
        return accountType;
    }

    public Account() {
    }

    //builder constructor
    public Account(Builder builder){
        this.accountID=builder.accountID;
        this.balance=builder.balance;
        this.password=builder.password;
        this.accountType=builder.accountType;
    }

    public static class Builder{
        private int accountID;
        private double balance;
        private String password;
        private String accountType;


        public Builder accountID(int value){
            this.accountID = value;
            return this;
        }

        public Builder balance(double value){
            this.balance = value;
            return this;
        }

        public Builder password(String value){
            this.password = value;
            return this;
        }

        public Builder accountType(String value){
            this.accountType = value;
            return this;
        }


        public Account build(){
            return new Account(this);
        }

    }
}
