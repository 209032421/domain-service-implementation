package tp_domain_project.Services;

import tp_domain_project.Domains.Account;

import java.util.Optional;
import java.util.Set;

public interface AccountService{
    public Account create(Account account);

    public Optional<Account> readByID(int accountID);

    public Set<Account> readAll();

    public Account update(Account account);

    public void delete(Account account);

}