package tp_domain_project.Services;

import tp_domain_project.Domains.Customer;

import java.util.Optional;
import java.util.Set;

public interface CustomerService{

    public Customer create(Customer customer);

    public Optional<Customer> readByID(int customerNumber);

    public Set<Customer> readAll();

    public Customer update(Customer customer);

    public void delete(Customer customerNumber);
}
