package tp_domain_project.Services.Impl;

import tp_domain_project.Domains.Customer;
import tp_domain_project.Repositories.CustomerRepo;
import tp_domain_project.Services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepo customerRepo;

    @Override
    public Customer create(Customer customer) {
        return customerRepo.save(customer);
    }

    public Optional<Customer> readByID(int customerNumber) {return customerRepo.findById(customerNumber);}

    @Override
    public Set<Customer> readAll()
    {
        Iterable<Customer> customers = customerRepo.findAll();
        Set customerSet = new HashSet();

        for(Customer customer:customers)
        {
            customerSet.add(customer);
        }
        return customerSet;
    }

    @Override
    public Customer update(Customer customer) {

        return customerRepo.save(customer);
    }

    @Override
    public void delete(Customer id) {

        customerRepo.delete(id);
    }

}
