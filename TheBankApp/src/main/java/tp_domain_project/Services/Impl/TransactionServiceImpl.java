package tp_domain_project.Services.Impl;


import tp_domain_project.Domains.Transaction;
import tp_domain_project.Repositories.TransactionRepo;
import tp_domain_project.Services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepo transactionRepo;

    @Override
    public Transaction create(Transaction transaction) {
        return transactionRepo.save(transaction);
    }

    public Optional<Transaction> readByID(int transactionID) {return transactionRepo.findById(transactionID);}

    @Override
    public Set<Transaction> readAll()
    {
        Iterable<Transaction> transactions = transactionRepo.findAll();
        Set transactionSet = new HashSet();

        for(Transaction transaction:transactions)
        {
            transactionSet.add(transaction);
        }
        return transactionSet;
    }

}
