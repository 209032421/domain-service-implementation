package tp_domain_project.Services.Impl;


import tp_domain_project.Domains.Account;
import tp_domain_project.Repositories.AccountRepo;
import tp_domain_project.Services.AccountService;


import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


public class AccountServiceImpl implements AccountService {


    private AccountRepo accountRepo;

    @Override
    public Account create(Account account) {
        return accountRepo.save(account);
    }

    public Optional<Account> readByID(int accountID) {return accountRepo.findById(accountID);}

    @Override
    public Set<Account> readAll()
    {
        Iterable<Account> accounts = accountRepo.findAll();
        Set accountSet = new HashSet();

        for(Account account:accounts)
        {
            accountSet.add(account);
        }
        return accountSet;
    }

    @Override
    public Account update(Account account) {

        return accountRepo.save(account);
    }

    @Override
    public void delete(Account id) {

        accountRepo.delete(id);
    }
}
