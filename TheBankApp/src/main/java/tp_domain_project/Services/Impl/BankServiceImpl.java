package tp_domain_project.Services.Impl;

import tp_domain_project.Domains.Bank;
import tp_domain_project.Repositories.BankRepo;
import tp_domain_project.Services.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class BankServiceImpl implements BankService {

    @Autowired
    private BankRepo bankRepo;

    @Override
    public Bank create(Bank bank) {
        return bankRepo.save(bank);
    }

    public Optional<Bank> readByID(int branchCode) {return bankRepo.findById(branchCode);}

    @Override
    public Set<Bank> readAll()
    {
        Iterable<Bank> banks = bankRepo.findAll();
        Set bankSet = new HashSet();

        for(Bank bank:banks)
        {
            bankSet.add(bank);
        }
        return bankSet;
    }

    @Override
    public Bank update(Bank bank) {

        return bankRepo.save(bank);
    }

    @Override
    public void delete(Bank branchCode) {

        bankRepo.delete(branchCode);
    }

}
