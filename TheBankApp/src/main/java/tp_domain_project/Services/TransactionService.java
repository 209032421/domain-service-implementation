package tp_domain_project.Services;

import tp_domain_project.Domains.Transaction;

import java.util.Optional;
import java.util.Set;

public interface TransactionService{

    public Transaction create(Transaction transaction);

    public Optional<Transaction> readByID(int transactionID);

    public Set<Transaction> readAll();

}
