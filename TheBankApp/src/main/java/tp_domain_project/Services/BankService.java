package tp_domain_project.Services;

import tp_domain_project.Domains.Bank;

import java.util.Optional;
import java.util.Set;

public interface BankService{

    public Bank create(Bank bank);

    public Optional<Bank> readByID(int branchCode);

    public Set<Bank> readAll();

    public Bank update(Bank bank);

    public void delete(Bank branchCode);
}
