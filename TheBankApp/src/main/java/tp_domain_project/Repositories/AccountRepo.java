package tp_domain_project.Repositories;

import tp_domain_project.Domains.Account;

import java.util.Optional;

public interface AccountRepo{
    Account create (Account account);
    Account read (int accountID);
    Account update (Account account);
    Account delete (Account accountID);

    Account save(Account account);

    Iterable<Account> findAll();

    Optional<Account> findById(int accountID);
}
