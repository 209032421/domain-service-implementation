package tp_domain_project.Repositories;


import tp_domain_project.Domains.Transaction;

import java.util.Optional;

public interface TransactionRepo{
    Transaction create (Transaction transaction);
    Transaction read (int transactionID);
    Transaction update (Transaction transaction);
    Transaction delete (int transactionID);

    Transaction save(Transaction transaction);

    Optional<Transaction> findById(int transactionID);

    Iterable<Transaction> findAll();
}
