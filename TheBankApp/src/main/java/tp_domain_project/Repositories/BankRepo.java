package tp_domain_project.Repositories;

import tp_domain_project.Domains.Bank;

import java.util.Optional;

public interface BankRepo {
    Bank create (Bank bank);
    Bank read (int branchCode);
    Bank update (Bank bank);
    Bank delete (Bank branchCode);

    Bank save(Bank bank);

    Optional<Bank> findById(int branchCode);

    Iterable<Bank> findAll();
}
