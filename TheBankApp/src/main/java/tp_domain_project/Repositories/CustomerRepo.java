package tp_domain_project.Repositories;

import tp_domain_project.Domains.Customer;

import java.util.Optional;


public interface CustomerRepo{
    Customer create (Customer customer);
    Customer read (int customerNumber);
    Customer update (Customer customer);
    Customer delete (Customer customerNumber);

    Customer save(Customer customer);

    Optional<Customer> findById(int customerNumber);

    Iterable<Customer> findAll();
}
