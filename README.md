Domain-Service Implementation
Using at least 5 domain classes from your selected domain model, write suitable repositories, factories and service that implement CRUD. Use a data structure to persist data for now.

NB: These five (5) domain classes MUST based on your selected domain model.

